package com.example.kwejk.model;

import java.util.List;

public interface GifDao {
 public List<Gif> findAll();
 public List<Gif> findFavorites();
 public Gif findOne(String name);
 public List<Category> showCategories();
 public Category findCategoryById(int id);
 public List<Gif> findByCategory(int id);
 public List<Gif> getSearchByName(String name);

}
