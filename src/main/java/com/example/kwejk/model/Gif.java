package com.example.kwejk.model;

public class Gif {


    private String name;
    private String username;
    private boolean isFavorite;
    Category category;

    public Gif() {
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Gif(String name, String username, boolean isFavorite, Category category) {
        this.name = name;
        this.username = username;
        this.isFavorite = isFavorite;
        this.category = category;
    }

    public Gif(String name, String username) {
        this.name = name;
        this.username = username;
    }

    public Gif(String name, String username, boolean isFavorite) {
        this.name = name;
        this.username = username;
        this.isFavorite = isFavorite;
    }

    public String getName() {
        return name;
    }

    public void setNameOfImage(String name) {
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isFavorite() {
        return isFavorite;
    }



    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }



    public void setFavorite(boolean isFavorite) {
        this.isFavorite = isFavorite;
    }



    public String getURL() {
        return getName() + ".gif";
    }
}