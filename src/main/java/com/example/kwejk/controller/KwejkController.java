package com.example.kwejk.controller;


import com.example.kwejk.model.Category;
import com.example.kwejk.model.GifDao;
import com.example.kwejk.model.GifDaoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import javax.jws.WebParam;

@Controller
public class KwejkController {


    GifDaoImpl gifDao = new GifDaoImpl();

    @GetMapping("/")
    public String getNameOfGif(ModelMap modelMap) {
        modelMap.put("gifs", gifDao.findAll());
        return "home";
    }

    @GetMapping("/favorites")
    public String getFavorites(ModelMap modelMap) {
        modelMap.put("favorites", gifDao.findFavorites());
        return "favorites";
    }

    @GetMapping("gif/{name}")
    public String details(@PathVariable String name, ModelMap modelMap) {

        modelMap.addAttribute("gif", gifDao.findOne(name));
        return "gif-details";
    }

    @GetMapping("/categories")
    public String showCat(ModelMap modelMap) {

        modelMap.addAttribute("categories", gifDao.showCategories());

        return ("categories");
    }


    @GetMapping("/category/{id}")
    public String showCategory(@PathVariable("id") int id, ModelMap modelMap) {
        modelMap.addAttribute("category", gifDao.findCategoryById(id));
        modelMap.addAttribute("gifsWithCategory", gifDao.findByCategory(id));
        return "category";
    }

    @GetMapping("/search")
    public String searchByname(@RequestParam String q, ModelMap modelMap) {
        modelMap.addAttribute("g", gifDao.getSearchByName(q));
        boolean isEmpty = gifDao.getSearchByName(q).isEmpty() ? true : false;
        modelMap.addAttribute("empty", isEmpty);
        return "search";
    }
}